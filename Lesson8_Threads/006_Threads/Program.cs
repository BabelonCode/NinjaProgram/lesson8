﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _006_Threads
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t = new Thread(Worker);

            // Превращение потока в фоновый    
            t.IsBackground = false;
            t.Start();

            Console.WriteLine("Returning from Main");
        }

        private static void Worker()
        {
            Thread.Sleep(100);
            Console.ForegroundColor = ConsoleColor.Yellow;

            //10 Second
            //Thread.Sleep(10000);
            for (int i = 0; i < 160; i++)
            {
                Thread.Sleep(20);
                Console.Write(".");
            }
            Console.WriteLine("Returning from Worker");
        }
    }
}